import { HttpStatuses } from "../application/enums/http-statuses.enum";
import HttpException from "../application/exceptions/http-exception";

import { AppDataSource } from "../configs/database/data-source";
import { Course } from "./entities/course.entity";
import { ICourse } from "./types/course.interface";
import { Mark } from "../marks/entities/mark.entity";
import { IMarkByCourse } from "../marks/types/student-mark.interface copy";

const coursesRepository = AppDataSource.getRepository(Course);
const marksRepository = AppDataSource.getRepository(Mark);

export const getAllCourses = async (lectorId?: string): Promise<Course[]> => {
  let queryBuilder = coursesRepository
    .createQueryBuilder("course")
    .leftJoinAndSelect("course.lectors", "lector");

  if (lectorId) {
    queryBuilder = queryBuilder.where("lector.id = :lectorId", { lectorId });
  }

  const courses = await queryBuilder
    .select([
      "course.created_at AS createdAt",
      "course.updated_at AS updatedAt",
      "course.id AS id",
      "course.name AS name",
      "course.description AS description",
      "course.hours AS hours",
    ])
    .getRawMany();

  return courses;
};

export const getMarksByCourseId = async (
  id: string
): Promise<IMarkByCourse[]> => {
  const marks = await marksRepository
    .createQueryBuilder("mark")
    .innerJoinAndSelect("mark.course", "course")
    .innerJoinAndSelect("mark.student", "student")
    .innerJoinAndSelect("mark.lector", "lector")
    .where("mark.courseId = :id", { id })
    .select([
      "course.name AS courseName",
      "lector.name AS lectorName",
      "student.name AS studentName",
      "mark.mark AS mark",
    ])
    .getRawMany();

  return marks;
};

export const createCourse = async (
  createCourseSchema: Omit<ICourse, "id">
): Promise<Course> => {
  const course = await coursesRepository.findOne({
    where: {
      name: createCourseSchema.name,
    },
  });
  if (course) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      "Course with this name already exists"
    );
  }

  return coursesRepository.save(createCourseSchema);
};
