export interface ICourse {
    id: string;
    description: string;
    name: string;
    hours: number;
  }
  