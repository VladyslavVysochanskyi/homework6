import { Request, Response } from "express";
import { ValidatedRequest } from "express-joi-validation";
import * as coursesService from "./courses.service";

import { ICourseCreateRequest } from "./types/course-create-request.interface";

type ReqQuery = { lectorId?: string };
export const getAllCourses = async (
  request: Request<ReqQuery>,
  response: Response
) => {
  const { lectorId } = request.query;
  const courses = await coursesService.getAllCourses(
    lectorId as string | undefined
  );
  response.json(courses);
};

export const getMarksByCourseId = async (
  request: Request<{ id: string }>,
  response: Response
) => {
  const { id } = request.params;
  const marks = await coursesService.getMarksByCourseId(id);
  response.json(marks);
};

export const createCourse = async (
  request: ValidatedRequest<ICourseCreateRequest>,
  response: Response
) => {
  const course = await coursesService.createCourse(request.body);
  response.json(course);
};
