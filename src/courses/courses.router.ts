import { Router } from "express";
import controllerWrapper from "../application/utilities/controller-wrapper";
import validator from "../application/middlewares/validation.middleware";
import { idParamSchema } from "../application/schemas/id-param.schema";

import * as coursesController from "./courses.controller";
import { courseCreateSchema } from "./course.schema";

const router = Router();

router.get("/", controllerWrapper(coursesController.getAllCourses));

router.get(
  "/:id/marks",
  validator.params(idParamSchema),
  controllerWrapper(coursesController.getMarksByCourseId)
);

router.post(
  "/",
  validator.body(courseCreateSchema),
  controllerWrapper(coursesController.createCourse)
);

export default router;
