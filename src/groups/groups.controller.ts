import { Request, Response } from "express";
import * as groupsService from "./groups.service";
import { ValidatedRequest } from "express-joi-validation";
import { IGroupCreateRequest } from "./types/group-create-request.interface";
import { IGroupUpdateRequest } from "./types/group-update-request.interface";

export const getGroupById = async (
  request: Request<{ id: string }>,
  response: Response
) => {
  const groupwithStudents = await groupsService.getGroupById(request.params.id);
  response.json(groupwithStudents);
};

export const getAllGroups = async (request: Request, response: Response) => {
  const groups = await groupsService.getAllGroups();
  response.json(groups);
};

export const createGroup = async (
  request: ValidatedRequest<IGroupCreateRequest>,
  response: Response
) => {
  const group = await groupsService.createGroup(request.body);
  response.json(group);
};

export const updateGroupById = async (
  request: ValidatedRequest<IGroupUpdateRequest>,
  response: Response
) => {
  const { id } = request.params;
  const group = await groupsService.updateGroupById(id, request.body);
  response.json(group);
};

export const deleteGroupById = async (
  request: Request<{ id: string }>,
  response: Response
) => {
  const { id } = request.params;
  const group = await groupsService.deleteGroupById(id);
  response.json(group);
};
