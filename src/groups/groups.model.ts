import ObjectID from "bson-objectid";

import { IGroup } from "./types/group.interface";
import { IStudent } from "../students/types/student.interface";
import * as studentsModel from "../students/students.model";

const groups: IGroup[] = [
  {
    id: ObjectID().toHexString(),
    name: "Group 1",
  },
  {
    id: ObjectID().toHexString(),
    name: "Group 2",
  },
  {
    id: "123456781234567812345678",
    name: "Group 3",
  },
];

export const getAllGroups = (): IGroup[] => {
  return groups;
};

export const getAllGroupsWithStudents = (): Array<
  IGroup & { students: IStudent[] }
> => {
  return groups.map((group) => {
    const students = studentsModel
      .getAllStudents()
      .filter((student) => student.groupId === group.id);
    return {
      ...group,
      students,
    };
  });
};

export const getGroupById = (groupId: string): IGroup | undefined => {
  return groups.find(({ id }) => id === groupId);
};

export const getGroupByIdWithStudents = (
  groupId: string
): (IGroup & { students: IStudent[] }) | undefined => {
  const group = getGroupById(groupId);

  if (!group) {
    return;
  }

  const students = studentsModel
    .getAllStudents()
    .filter((student) => student.groupId === group.id);

  return {
    ...group,
    students,
  };
};

export const createGroup = (createGroupSchema: Omit<IGroup, "id">): IGroup => {
  const newGroup = {
    ...createGroupSchema,
    id: ObjectID().toHexString(),
  };
  groups.push(newGroup);
  return newGroup;
};

export const updateGroupById = (
  groupId: string,
  updateGroupSchema: Partial<IGroup>
): IGroup | undefined => {
  const groupIndex = groups.findIndex(({ id }) => id === groupId);
  const group = groups[groupIndex];
  if (!group) {
    return;
  }
  const updatedGroup = {
    ...group,
    ...updateGroupSchema,
  };
  groups.splice(groupIndex, 1, updatedGroup);
  return updatedGroup;
};

export const deleteGroupById = (groupId: string): IGroup | undefined => {
  const groupIndex = groups.findIndex(({ id }) => id === groupId);
  const group = groups[groupIndex];
  if (!group) {
    return;
  }

  studentsModel.getAllStudents().forEach((student) => {
    if (student.groupId === groupId) {
      student.groupId = undefined;
    }
  });

  groups.splice(groupIndex, 1);
  return group;
};
