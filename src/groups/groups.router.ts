import express from "express";
import { groupCreateSchema, groupUpdateSchema } from "./group.schema";
import * as groupsController from "./groups.controller";
import { idParamSchema } from "../application/schemas/id-param.schema";
import validator from "../application/middlewares/validation.middleware";
import controllerWrapper from "../application/utilities/controller-wrapper";

const router = express.Router();

router.get("/", controllerWrapper(groupsController.getAllGroups));

router.get(
  "/:id",
  validator.params(idParamSchema),
  controllerWrapper(groupsController.getGroupById)
);

router.post(
  "/",
  validator.body(groupCreateSchema),
  controllerWrapper(groupsController.createGroup)
);

router.delete(
  "/:id",
  validator.params(idParamSchema),
  controllerWrapper(groupsController.deleteGroupById)
);

router.patch(
  "/:id",
  validator.params(idParamSchema),
  validator.body(groupUpdateSchema),
  controllerWrapper(groupsController.updateGroupById)
);

export default router;
