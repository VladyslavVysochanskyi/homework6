import { DeleteResult, UpdateResult } from "typeorm";
import { HttpStatuses } from "../application/enums/http-statuses.enum";
import HttpException from "../application/exceptions/http-exception";
import { AppDataSource } from "../configs/database/data-source";
import { Group } from "./entities/group.entity";
import { IGroup } from "./types/group.interface";

const groupsRepository = AppDataSource.getRepository(Group);

export const getAllGroups = async (): Promise<Group[]> => {
  const groups = await groupsRepository
    .createQueryBuilder("group")
    .leftJoinAndSelect("group.students", "student")
    .select([
      "group.id",
      "group.name",
      "student.id",
      "student.name",
      "student.surname",
      "student.email",
      "student.age",
    ])
    .getMany();
  return groups;
};

export const getGroupById = async (id: string): Promise<Group> => {
  const group = await groupsRepository
    .createQueryBuilder("group")
    .leftJoinAndSelect("group.students", "student")
    .select([
      "group.id",
      "group.name",
      "student.id",
      "student.name",
      "student.surname",
      "student.email",
      "student.age",
    ])
    .where("group.id = :id", { id })
    .getOne();

  if (!group) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      "Group with this id not found"
    );
  }

  return group;
};

export const createGroup = async (
  createGroupSchema: Omit<IGroup, "id">
): Promise<Group> => {
  const group = await groupsRepository.findOne({
    where: {
      name: createGroupSchema.name,
    },
  });
  if (group) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      "Group with this name already exists"
    );
  }

  return groupsRepository.save(createGroupSchema);
};

export const deleteGroupById = async (id: string): Promise<DeleteResult> => {
  const result = await groupsRepository.delete(id);
  if (!result.affected) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      "Group with this id not found"
    );
  }
  return result;
};

export const updateGroupById = async (
  id: string,
  updateGroupSchema: Partial<IGroup>
): Promise<UpdateResult> => {
  const result = await groupsRepository.update(id, updateGroupSchema);
  if (!result.affected) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      "Group with this id not found"
    );
  }
  return result;
};
