import express from "express";
import cors from "cors";
import bodyParser from "body-parser";

import { STATIC_FILES } from "../application/paths.const";

import logger from "./middlewares/logger.middleware";
import exceptionsFilter from "./middlewares/exceptions.filter";
// import auth from "./middlewares/auth.middleware";

import { AppDataSource } from "../configs/database/data-source";

import studentsRouter from "../students/students.router";
import groupsRouter from "../groups/groups.router";
import lectorsRouter from "../lectors/lectors.router";
import coursesRouter from "../courses/courses.router";
import marksRouter from "../marks/marks.router";

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(logger);
// app.use(auth);

AppDataSource.initialize()
  .then(() => {
    console.log("Typeorm connected to database");
  })
  .catch((error) => {
    console.log("Error: ", error);
  });

app.use("/api/v1/public", express.static(STATIC_FILES));
app.use("/api/v1/students", studentsRouter);
app.use("/api/v1/groups", groupsRouter);
app.use("/api/v1/lectors", lectorsRouter);
app.use("/api/v1/courses", coursesRouter);
app.use("/api/v1/marks", marksRouter);

app.use(exceptionsFilter);

export default app;
