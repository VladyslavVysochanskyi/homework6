import {
  BaseEntity,
  CreateDateColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

export abstract class CoreEntity extends BaseEntity {
  @PrimaryGeneratedColumn("uuid")
  public id: string;
  @CreateDateColumn({ type: "timestamp with time zone", name: "created_at" })
  public cratedAt: Date;
  @UpdateDateColumn({ type: "timestamp with time zone", name: "updated_at" })
  public updateAt: Date;
}
