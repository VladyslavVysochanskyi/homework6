import ObjectID from "bson-objectid";

import { IStudent } from "./types/student.interface";
import * as groupsModel from "../groups/groups.model";

const students: IStudent[] = [
  {
    id: ObjectID().toHexString(),
    name: "Bill",
    surname: "Gates",
    age: 67,
    email: "billgates@email.com",
  },
  {
    id: ObjectID().toHexString(),
    name: "Steve",
    surname: "Jobs",
    age: 56,
    email: "stevejobs@email.com",
    groupId: "123456781234567812345678",
  },
  {
    id: "123456781234567812345678",
    name: "Steve",
    surname: "Jobs",
    age: 56,
    email: "stevejobs@email.com",
    groupId: "123456781234567812345678",
  },
];

export const getAllStudents = (): IStudent[] => {
  return students;
};

export const getAllStudentsWithGroup = (): Array<
  IStudent & { groupName?: string }
> => {
  return students.map((student) => {
    const group = student.groupId
      ? groupsModel.getGroupById(student.groupId)
      : undefined;
    const groupName = group ? group.name : undefined;
    return {
      ...student,
      groupName,
    };
  });
};

export const getStudentById = (studentId: string): IStudent | undefined => {
  return students.find(({ id }) => id === studentId);
};

export const getStudentByIdWithGroup = (
  studentId: string
): (IStudent & { groupName?: string }) | undefined => {
  const student = getStudentById(studentId);

  if (!student) {
    return;
  }

  const group = student.groupId
    ? groupsModel.getGroupById(student.groupId)
    : undefined;
  const groupName = group ? group.name : undefined;

  return {
    ...student,
    groupName,
  };
};

export const getStudentByEmail = (
  studentEmail: string
): IStudent | undefined => {
  return students.find(({ email }) => email === studentEmail);
};

export const createStudent = (
  createStudentSchema: Omit<IStudent, "id">
): IStudent => {
  const newStudent = {
    ...createStudentSchema,
    id: ObjectID().toHexString(),
  };
  students.push(newStudent);
  return newStudent;
};

export const updateStudentById = (
  studentId: string,
  updateStudentSchema: Partial<IStudent>
): IStudent | undefined => {
  const studentIndex = students.findIndex(({ id }) => id === studentId);
  const student = students[studentIndex];
  if (!student) {
    return;
  }

  const updatedStudent = {
    ...student,
    ...updateStudentSchema,
  };

  students.splice(studentIndex, 1, updatedStudent);
  return updatedStudent;
};

export const deleteStudentById = (studentId: string): IStudent | undefined => {
  const studentIndex = students.findIndex(({ id }) => id === studentId);
  const student = students[studentIndex];
  if (!student) {
    return;
  }
  students.splice(studentIndex, 1);
  return student;
};

export const addGroupToStudent = (studentId: string, groupId: string) => {
  const student = getStudentById(studentId);
  if (!student) {
    return;
  }

  student.groupId = groupId;
  return student;
};
