import { Column, Entity, Index, JoinColumn, ManyToOne, OneToMany } from "typeorm";
import { CoreEntity } from "../../application/entities/core.entity";
import { Status } from "../enums/status.enum";
import { Group } from "../../groups/entities/group.entity";
import { Mark } from "../../marks/entities/mark.entity";

@Entity({ name: "students" })
@Index(["name"])
export class Student extends CoreEntity {
  @Column({
    type: "varchar",
    nullable: false,
  })
  name: string;

  @Column({
    type: "varchar",
    nullable: false,
  })
  surname: string;

  @Column({
    type: "varchar",
    nullable: false,
  })
  email: string;

  @Column({
    type: "numeric",
    nullable: false,
  })
  age: number;

  @Column({
    type: "varchar",
    nullable: true,
    name: "image_path",
  })
  imagePath: string;

  // @Column({
  //   type: "enum",
  //   enum: Status,
  //   default: Status.ACTIVE,
  //   nullable: false,
  // })
  // status: Status;

  @ManyToOne(() => Group, (group) => group.students, {
    nullable: true,
    eager: false,
  })
  @JoinColumn({ name: "group_id" })
  group: Group;

  @Column({
    type: "varchar",
    nullable: true,
    name: "group_id",
  })
  groupId: string;

  @OneToMany(() => Mark, (mark) => mark.student, {
    nullable: true,
    eager: false,
  })
  marks: Mark[];
}
