import { Request, Response } from "express";
import { ValidatedRequest } from "express-joi-validation";
import path from "path";
import { STATIC_FILES } from "../application/paths.const";
import * as studentsService from "./studetns.service";
import { IStudentUpdateRequest } from "./types/student-update-request.interface";
import { IStudentCreateRequest } from "./types/student-create-request.interface";
import { IStudentAddGroupRequest } from "./types/student-add-group-request.interface";

type ReqQuery = { name?: string };
export const getAllStudents = async (
  request: Request<ReqQuery>,
  response: Response
) => {
  const { name } = request.query;
  const students = await studentsService.getAllStudents(
    name as string | undefined
  );
  response.json(students);
};

export const getStudentById = async (
  request: Request<{ id: string }>,
  response: Response
) => {
  const { id } = request.params;
  const student = await studentsService.getStudentById(id);
  response.json(student);
};

export const getMarksByStudentId = async (
  request: Request<{ id: string }>,
  response: Response
) => {
  const { id } = request.params;
  const marks = await studentsService.getMarksByStudentId(id);
  response.json(marks);
};

export const createStudent = async (
  request: ValidatedRequest<IStudentCreateRequest>,
  response: Response
) => {
  const student = await studentsService.createStudent(request.body);
  response.json(student);
};

export const deleteStudentById = async (
  request: Request<{ id: string }>,
  response: Response
) => {
  const { id } = request.params;
  const student = await studentsService.deleteStudentById(id);
  response.json(student);
};

export const updateStudentById = async (
  request: ValidatedRequest<IStudentUpdateRequest>,
  response: Response
) => {
  const { id } = request.params;
  const student = await studentsService.updateStudentById(id, request.body);
  response.json(student);
};

export const addGroupToStudent = async (
  request: ValidatedRequest<IStudentAddGroupRequest>,
  response: Response
) => {
  const { id } = request.params;
  const { groupId } = request.body;
  const student = await studentsService.addGroupToStudent(id, groupId);
  response.json(student);
};

export const addImage = async (
  request: Request<{ id: string; file: Express.Multer.File }>,
  response: Response
) => {
  const { id } = request.params;
  const { path } = request.file ?? {};
  const updatedStudent = await studentsService.addImage(id, path);
  response.json(updatedStudent);
};

export const getImageById = (
  request: Request<{ id: string }>,
  response: Response
) => {
  const { id } = request.params;
  const imagePath = studentsService.getImageById(id);
  response.sendFile(path.join(STATIC_FILES, imagePath));
};
