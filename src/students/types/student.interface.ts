import { Status } from "../enums/status.enum";

export interface IStudent {
  id: string;
  email: string;
  name: string;
  surname: string;
  age: number;
  // status: Status;
  imagePath?: string;
  groupId?: string;
}
