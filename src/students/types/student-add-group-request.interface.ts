import { ContainerTypes, ValidatedRequestSchema } from "express-joi-validation";
import { IStudent } from "./student.interface";

export interface IStudentAddGroupRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Required<Pick<IStudent, "groupId">>;
}
