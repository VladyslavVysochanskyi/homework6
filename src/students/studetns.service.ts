import ObjectID from "bson-objectid";
import path from "path";
import fs from "fs/promises";
import { HttpStatuses } from "../application/enums/http-statuses.enum";
import HttpException from "../application/exceptions/http-exception";
import * as studentsModel from "./students.model";
import { IStudent } from "./types/student.interface";
import { Student } from "./entities/student.entity";
import { AppDataSource } from "../configs/database/data-source";
import { DeleteResult, UpdateResult } from "typeorm";
import { Mark } from "../marks/entities/mark.entity";
import { IMarkByStudent } from "../marks/types/student-mark.interface";

const studentsRepository = AppDataSource.getRepository(Student);
const marksRepository = AppDataSource.getRepository(Mark);

export const getAllStudents = async (name?: string): Promise<Student[]> => {
  let queryBuilder = await studentsRepository.createQueryBuilder("student");

  if (name) {
    queryBuilder = queryBuilder.where("student.name = :name", { name });
  }

  const students = await queryBuilder
    .select([
      "student.id as id",
      "student.created_at as createdAt",
      "student.updated_at as updatedAt",
      "student.name as name",
      "student.surname as surname",
      "student.email as email",
      "student.age as age",
      "student.image_path as imagePath",
      "student.group_id as groupId",
    ])
    .leftJoin("student.group", "group")
    .addSelect("group.name", "groupName")
    .getRawMany();

  return students;
};

export const getStudentById = async (id: string): Promise<Student> => {
  const student = await studentsRepository
    .createQueryBuilder("student")
    .select([
      "student.id as id",
      "student.created_at as createdAt",
      "student.updated_at as updatedAt",
      "student.name as name",
      "student.surname as surname",
      "student.email as email",
      "student.age as age",
      "student.image_path as imagePath",
      "student.group_id as groupId",
    ])
    .leftJoin("student.group", "group")
    .addSelect('group.name as "groupName"')
    .where("student.id = :id", { id })
    .getRawOne();

  if (!student) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      "Student with this id not found"
    );
  }
  return student;
};

export const getMarksByStudentId = async (
  id: string
): Promise<IMarkByStudent[]> => {
  const marks = await marksRepository
    .createQueryBuilder("mark")
    .innerJoinAndSelect("mark.course", "course")
    .innerJoinAndSelect("mark.student", "student")
    .where("mark.studentId = :id", { id })
    .select(["course.name AS courseName", "mark.mark AS mark"])
    .getRawMany();

  return marks;
};

export const createStudent = async (
  createStudentSchema: Omit<IStudent, "id">
): Promise<Student> => {
  const student = await studentsRepository.findOne({
    where: {
      email: createStudentSchema.email,
    },
  });
  if (student) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      "Student with this email already exists"
    );
  }

  return studentsRepository.save(createStudentSchema);
};

export const deleteStudentById = async (id: string): Promise<DeleteResult> => {
  const result = await studentsRepository.delete(id);
  if (!result.affected) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      "Student with this id not found"
    );
  }
  return result;
};

export const updateStudentById = async (
  id: string,
  updateStudentSchema: Partial<IStudent>
): Promise<UpdateResult> => {
  const result = await studentsRepository.update(id, updateStudentSchema);
  if (!result.affected) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      "Student with this id not found"
    );
  }
  return result;
};

export const addGroupToStudent = async (studentId: string, groupId: string) => {
  const result = await studentsRepository.update(studentId, { groupId });
  if (!result.affected) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      "Student with this id not found"
    );
  }
  return result;
};

export const addImage = async (id: string, filePath?: string) => {
  if (!filePath) {
    throw new HttpException(HttpStatuses.BAD_REQUEST, "File is not provided");
  }
  try {
    const imageld = ObjectID().toHexString();
    const imageExtention = path.extname(filePath);
    const imageName = imageld + imageExtention;
    const studentsDirectoryName = "students";
    const studentsDirectoryPath = path.join(
      __dirname,
      "../",
      "public",
      studentsDirectoryName
    );
    const newImagePath = path.join(studentsDirectoryPath, imageName);
    const imagePath = `${studentsDirectoryName}/${imageName}`;
    await fs.rename(filePath, newImagePath);
    const updatedStudent = updateStudentById(id, { imagePath });
    return updatedStudent;
  } catch (error) {
    await fs.unlink(filePath);
    return new HttpException(HttpStatuses.BAD_REQUEST, "File is not provided");
  }
};

export const getImageById = (id: string) => {
  const student = studentsModel.getStudentById(id);
  if (!student || !student.imagePath) {
    throw new HttpException(HttpStatuses.NOT_FOUND, "Image not found");
  }

  return student.imagePath;
};
