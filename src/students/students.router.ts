import { Router } from "express";
import controllerWrapper from "../application/utilities/controller-wrapper";
import uploadMiddleware from "../application/middlewares/upload.middleware";
import validator from "../application/middlewares/validation.middleware";
import { idParamSchema } from "../application/schemas/id-param.schema";
import {
  studentCreateSchema,
  studentUpdateSchema,
  studentAddGroupSchema,
} from "./student.schema";
import * as studentsController from "./students.controller";

const router = Router();

router.get("/", controllerWrapper(studentsController.getAllStudents));

router.get(
  "/:id",
  validator.params(idParamSchema),
  controllerWrapper(studentsController.getStudentById)
);

router.get(
  "/:id/marks",
  validator.params(idParamSchema),
  controllerWrapper(studentsController.getMarksByStudentId)
);

router.post(
  "/",
  validator.body(studentCreateSchema),
  controllerWrapper(studentsController.createStudent)
);

router.delete(
  "/:id",
  validator.params(idParamSchema),
  controllerWrapper(studentsController.deleteStudentById)
);

router.patch(
  "/:id",
  validator.params(idParamSchema),
  validator.body(studentUpdateSchema),
  controllerWrapper(studentsController.updateStudentById)
);

router.patch(
  "/:id/group",
  validator.params(idParamSchema),
  validator.body(studentAddGroupSchema),
  controllerWrapper(studentsController.addGroupToStudent)
);

router.patch(
  "/:id/image",
  validator.params(idParamSchema),
  uploadMiddleware.single("file"),
  controllerWrapper(studentsController.addImage)
);

router.get(
  "/:id/image",
  validator.params(idParamSchema),
  studentsController.getImageById
);

export default router;
