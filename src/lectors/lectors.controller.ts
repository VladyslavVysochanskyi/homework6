import { Request, Response } from "express";
import { ValidatedRequest } from "express-joi-validation";
import * as lectorsService from "./lectors.service";

import { ILectorCreateRequest } from "./types/lector-create-request.interface";
import { ILectorAddCourseRequest } from "./types/lector-add-course-request.interface";

export const getAllLectors = async (request: Request, response: Response) => {
  const lectors = await lectorsService.getAllLectors();
  response.json(lectors);
};

export const getLectorById = async (
  request: Request<{ id: string }>,
  response: Response
) => {
  const lector = await lectorsService.getLectorById(request.params.id);
  response.json(lector);
};

export const createLector = async (
  request: ValidatedRequest<ILectorCreateRequest>,
  response: Response
) => {
  const lector = await lectorsService.createLector(request.body);
  response.json(lector);
};

export const addCourseToLector = async (
  request: ValidatedRequest<ILectorAddCourseRequest>,
  response: Response
) => {
  const { id } = request.params;
  const { courseId } = request.body;
  const lector = await lectorsService.addCourseToLector(id, courseId);
  response.json(lector);
};
