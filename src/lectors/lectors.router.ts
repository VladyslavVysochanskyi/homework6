import { Router } from "express";
import controllerWrapper from "../application/utilities/controller-wrapper";
import validator from "../application/middlewares/validation.middleware";
import { idParamSchema } from "../application/schemas/id-param.schema";
import { lectorAddCourseSchema, lectorCreateSchema } from "./lector.schema";
import * as lectorsController from "./lectors.controller";

const router = Router();

router.get("/", controllerWrapper(lectorsController.getAllLectors));

router.get(
  "/:id",
  validator.params(idParamSchema),
  controllerWrapper(lectorsController.getLectorById)
);

router.post(
  "/",
  validator.body(lectorCreateSchema),
  controllerWrapper(lectorsController.createLector)
);

router.patch(
  "/:id/courses",
  validator.params(idParamSchema),
  validator.body(lectorAddCourseSchema),
  controllerWrapper(lectorsController.addCourseToLector)
);

export default router;
