import { ContainerTypes, ValidatedRequestSchema } from "express-joi-validation";
import { ICoursesLectorsLectors } from "./courses_lectors_lectors.interface";

export interface ILectorAddCourseRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Omit<ICoursesLectorsLectors, "lectorId">;
}
