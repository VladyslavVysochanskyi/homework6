import { Entity, Column, ManyToMany, OneToMany } from "typeorm";
import { CoreEntity } from "../../application/entities/core.entity";
import { Course } from "../../courses/entities/course.entity";
import { Mark } from "../../marks/entities/mark.entity";

@Entity({ name: "lectors" })
export class Lector extends CoreEntity {
  @Column({
    type: "varchar",
    nullable: false,
  })
  name: string;

  @Column({
    type: "varchar",
    nullable: false,
  })
  email: string;

  @Column({
    type: "varchar",
    nullable: false,
  })
  password: string;

  @ManyToMany(() => Course, (course) => course.lectors)
  courses: Course[];

  @OneToMany(() => Mark, (mark) => mark.lector)
  marks: Mark[];
}
