import { HttpStatuses } from "../application/enums/http-statuses.enum";
import HttpException from "../application/exceptions/http-exception";
import { AppDataSource } from "../configs/database/data-source";

import { Lector } from "./entities/lector.entity";
import { ILector } from "./types/lector.interface";
import { Course } from "../courses/entities/course.entity";

const lectorsRepository = AppDataSource.getRepository(Lector);
const coursesRepository = AppDataSource.getRepository(Course);

export const getAllLectors = async (): Promise<Lector[]> => {
  const lectors = await lectorsRepository.find();
  return lectors;
};

export const getLectorById = async (id: string): Promise<Lector> => {
  const lector = await lectorsRepository
    .createQueryBuilder("lector")
    .leftJoinAndSelect("lector.courses", "course")
    .where("lector.id = :id", { id })
    .getOne();

  if (!lector) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      "Lector with this id not found"
    );
  }

  return lector;
};

export const createLector = async (
  createLectorSchema: Omit<ILector, "id">
): Promise<Lector> => {
  const lector = await lectorsRepository.findOne({
    where: {
      email: createLectorSchema.email,
    },
  });
  if (lector) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      "Lector with this email already exists"
    );
  }

  return lectorsRepository.save(createLectorSchema);
};

export const addCourseToLector = async (
  id: string,
  courseId: string
): Promise<Lector> => {
  const lector = await lectorsRepository.findOne({
    where: {
      id,
    },
    relations: {
      courses: true,
    },
  });

  const course = await coursesRepository.findOne({
    where: {
      id: courseId,
    },
  });

  if (!lector || !course) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      "Lector or course with this id not found"
    );
  }

  const isInCourses = lector.courses.some((course) => course.id === courseId);

  if (isInCourses) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      "Course with this id already added"
    );
  }

  if (!isInCourses) {
    lector.courses.push(course);
  }

  return await AppDataSource.manager.save(lector);
};
