import Joi from "joi";
import { ILector } from "./types/lector.interface";
import { ICoursesLectorsLectors } from "./types/courses_lectors_lectors.interface";

export const lectorCreateSchema = Joi.object<Omit<ILector, "id">>({
  name: Joi.string().required(),
  email: Joi.string().required(),
  password: Joi.string().required(),
});

export const lectorAddCourseSchema = Joi.object<
  Partial<ICoursesLectorsLectors>
>({
  courseId: Joi.string().uuid().length(36).required(),
});
