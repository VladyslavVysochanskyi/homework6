import { AppDataSource } from "../configs/database/data-source";
import { Mark } from "./entities/mark.entity";
import { IMark } from "./types/mark.interface";

const marksRepository = AppDataSource.getRepository(Mark);

export const createMark = async (
  createLectorCoursesSchema: Omit<IMark, "id">
): Promise<IMark> => {
  return marksRepository.save(createLectorCoursesSchema);
};
