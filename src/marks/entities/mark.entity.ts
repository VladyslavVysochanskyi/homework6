import { Entity, Column, ManyToOne, JoinColumn } from "typeorm";
import { CoreEntity } from "../../application/entities/core.entity";
import { Student } from "../../students/entities/student.entity";
import { Course } from "../../courses/entities/course.entity";
import { Lector } from "../../lectors/entities/lector.entity";

@Entity({ name: "marks" })
export class Mark extends CoreEntity {
  @Column({
    type: "numeric",
    nullable: false,
  })
  mark: number;

  @ManyToOne(() => Student, (student) => student.marks, {
    nullable: false,
    eager: false,
  })
  @JoinColumn({ name: "student_id" })
  student: Student;

  @Column({
    type: "varchar",
    nullable: false,
    name: "student_id",
  })
  studentId: string;

  @ManyToOne(() => Course, (course) => course.marks, {
    nullable: false,
    eager: false,
  })
  @JoinColumn({ name: "course_id" })
  course: Course;

  @Column({
    type: "varchar",
    nullable: false,
    name: "course_id",
  })
  courseId: string;

  @ManyToOne(() => Lector, (lector) => lector.marks, {
    nullable: false,
    eager: false,
  })
  @JoinColumn({ name: "lector_id" })
  lector: Lector;

  @Column({
    type: "varchar",
    nullable: false,
    name: "lector_id",
  })
  lectorId: string;
}
