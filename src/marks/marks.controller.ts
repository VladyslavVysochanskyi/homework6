import { Response } from "express";
import { ValidatedRequest } from "express-joi-validation";
import * as marksService from "./marks.service";
import { IMarkCreateRequest } from "./types/mark-create-request.interface";

export const createMark = async (
  request: ValidatedRequest<IMarkCreateRequest>,
  response: Response
) => {
  const mark = await marksService.createMark(request.body);
  response.json(mark);
};
