import Joi from "joi";
import { IMark } from "./types/mark.interface";

export const markCreateSchema = Joi.object<Omit<IMark, "id">>({
  courseId: Joi.string().uuid().length(36).required(),
  studentId: Joi.string().uuid().length(36).required(),
  lectorId: Joi.string().uuid().length(36).required(),
  mark: Joi.number().integer().positive().required(),
});
