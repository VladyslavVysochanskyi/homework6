export interface IMarkByCourse {
    mark: number;
    courseName: string;
    lectorName: string;
    studentName: string;
  }
  