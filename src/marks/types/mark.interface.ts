export interface IMark {
    id: string;
    mark: number;
    lectorId: string;
    courseId: string;
    studentId: string;
  }
  