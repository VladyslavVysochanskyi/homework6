import { Router } from "express";
import controllerWrapper from "../application/utilities/controller-wrapper";
import validator from "../application/middlewares/validation.middleware";
import { markCreateSchema } from "./marks.schema";
import * as marksController from "./marks.controller";

const router = Router();

router.post(
  "/",
  validator.body(markCreateSchema),
  controllerWrapper(marksController.createMark)
);

export default router;
